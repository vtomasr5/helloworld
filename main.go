package main

import (
	"fmt"
	"log"
	"net/http"
)

func helloworld(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Hello ArgoCD!")
}

func healthcheck(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
	fmt.Fprint(w)
}

func main() {
	http.HandleFunc("/", helloworld)
	http.HandleFunc("/hc", healthcheck)

	log.Printf("Listening at 0.0.0.0:8080\n")
	log.Fatal(http.ListenAndServe("0.0.0.0:8080", nil))
}
