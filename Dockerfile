FROM golang:1.14 AS builder
LABEL MAINTAINER "Vicenç Juan Tomas Monserrat <vtomasr5@gmail.com>"
WORKDIR /app
COPY . .
RUN CGO_ENABLED=0 GOOS=linux go build -o helloworld

# --------------
FROM scratch
COPY --from=builder /app/helloworld .
EXPOSE 8080
CMD [ "/helloworld" ]